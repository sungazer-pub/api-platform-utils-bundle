<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\DeadCode\Rector\ClassMethod\RemoveUselessReturnTagRector;
use Rector\DeadCode\Rector\Property\RemoveUselessVarTagRector;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;
use Rector\TypeDeclaration\Rector\ClassMethod\AddVoidReturnTypeWhereNoReturnRector;

return RectorConfig::configure()
                   ->withPaths([
                                 __DIR__ . '/src',
                               ])
  // uncomment to reach your current PHP version
                   ->withPhpSets(php82: true)
                   ->withRules([
                                 AddVoidReturnTypeWhereNoReturnRector::class,
                                 RemoveUselessReturnTagRector::class,
                                 RemoveUselessVarTagRector::class,
                                 \Rector\Symfony\Symfony61\Rector\Class_\CommandPropertyToAttributeRector::class,
                                 \Rector\Symfony\Symfony62\Rector\Class_\MessageHandlerInterfaceToAttributeRector::class,
                               ])
                   ->withSets([
                                DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES,
                                SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES,
                                SymfonySetList::SYMFONY_64,
                                SetList::TYPE_DECLARATION,
//                                  SymfonySetList::SYMFONY_CODE_QUALITY,
                              ]);
