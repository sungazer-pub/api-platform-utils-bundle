<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\JsonLd\Action;

use ApiPlatform\Core\JsonLd\Action\ContextAction as ApiContextAction;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedMetadataFactoryInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedNameCollectionFactoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Decorates Api Platform's JsonLd Context Action to add support for non-resource objects
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final readonly class ContextAction
{
    public function __construct(private ApiContextAction $decorated, private EmbeddedNameCollectionFactoryInterface $embeddedNameCollectionFactory, private EmbeddedMetadataFactoryInterface $embeddedMetadataFactory)
    {
    }

    /**
     * Generates a context according to the type requested.
     *
     * @throws NotFoundHttpException
     */
    public function __invoke(string $shortName): array
    {
        if ('Entrypoint' === $shortName) {
            $data = $this->decorated->__invoke($shortName);
            foreach ($this->embeddedNameCollectionFactory->create() as $embedded) {
                $metadata = $this->embeddedMetadataFactory->create($embedded);
                $lcShortName = lcfirst((string) $metadata->getShortName());
                $data['@context'][$lcShortName] = [
                    '@id' => "Entrypoint/$lcShortName",
                    '@type' => '@id',
                ];
            }
            return $data;
        }
        return $this->decorated->__invoke($shortName);
    }
}
