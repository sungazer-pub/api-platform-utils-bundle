<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use Psr\Cache\CacheItemPoolInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Cache\CachedTrait;

/**
 * Caches embedded name collection.
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final class CachedEmbeddedNameCollectionFactory implements EmbeddedNameCollectionFactoryInterface
{
    use CachedTrait;

    public const CACHE_KEY = 'embedded_name_collection';

    public function __construct(CacheItemPoolInterface                 $cacheItemPool,
                                private EmbeddedNameCollectionFactoryInterface $decorated)
    {
        $this->cacheItemPool = $cacheItemPool;
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function create(): array
    {
        return $this->getCached(self::CACHE_KEY, fn(): array => $this->decorated->create());
    }
}
