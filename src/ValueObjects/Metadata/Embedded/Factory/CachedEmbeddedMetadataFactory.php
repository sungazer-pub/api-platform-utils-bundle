<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use Psr\Cache\CacheItemPoolInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Cache\CachedTrait;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata;

/**
 * Caches embedded metadata.
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final class CachedEmbeddedMetadataFactory implements EmbeddedMetadataFactoryInterface
{
    use CachedTrait;

    public const CACHE_KEY_PREFIX = 'embedded_metadata_';

    public function __construct(CacheItemPoolInterface $cacheItemPool, private EmbeddedMetadataFactoryInterface $decorated)
    {
        $this->cacheItemPool = $cacheItemPool;
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function create(string $embeddedClass): EmbeddedMetadata
    {
        $cacheKey = self::CACHE_KEY_PREFIX . md5($embeddedClass);

        return $this->getCached($cacheKey, fn(): \Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata => $this->decorated->create($embeddedClass));
    }
}
