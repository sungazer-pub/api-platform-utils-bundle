<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use ApiPlatform\Core\Util\ReflectionClassRecursiveIterator;
use Doctrine\Common\Annotations\Reader;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Annotation\ApiEmbedded;

/**
 * Creates an embedded name collection from @ApiEmbedded annotations.
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
class AnnotationEmbeddedNameCollectionFactory implements EmbeddedNameCollectionFactoryInterface
{
    /**
     * @param string[] $paths
     */
    public function __construct(private readonly Reader $reader, private readonly array $paths, private readonly ?\Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedNameCollectionFactoryInterface $decorated = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function create(): array
    {
        $classes = [];

        if ($this->decorated) {
            foreach ($this->decorated->create() as $resourceClass) {
                $classes[$resourceClass] = true;
            }
        }

        foreach (ReflectionClassRecursiveIterator::getReflectionClassesFromDirectories($this->paths) as $className => $reflectionClass) {
            if ($this->reader->getClassAnnotation($reflectionClass, ApiEmbedded::class)) {
                $classes[$className] = true;
            }
        }

        return array_keys($classes);
    }
}
