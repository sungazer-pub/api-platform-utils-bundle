<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use Doctrine\Common\Annotations\Reader;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Annotation\ApiEmbedded;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata;

/**
 * Creates a resource metadata from {@see ApiEmbedded} annotations.
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final readonly class AnnotationEmbeddedMetadataFactory implements EmbeddedMetadataFactoryInterface
{
    public function __construct(private Reader $reader, private ?\Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedMetadataFactoryInterface $decorated = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function create(string $embeddedClass): EmbeddedMetadata
    {
        $parentResourceMetadata = null;
        if ($this->decorated) {
            try {
                $parentResourceMetadata = $this->decorated->create($embeddedClass);
            } catch (ResourceClassNotFoundException) {
                // Ignore not found exception from decorated factories
            }
        }

        try {
            $reflectionClass = new \ReflectionClass($embeddedClass);
        } catch (\ReflectionException) {
            return $this->handleNotFound($parentResourceMetadata, $embeddedClass);
        }

        $annotation = $this->reader->getClassAnnotation($reflectionClass, ApiEmbedded::class);
        if (!$annotation instanceof ApiEmbedded) {
            return $this->handleNotFound($parentResourceMetadata, $embeddedClass);
        }

        return $this->createMetadata($annotation, $parentResourceMetadata);
    }

    /**
     * Returns the metadata from the decorated factory if available or throws an exception.
     *
     * @throws ResourceClassNotFoundException
     */
    private function handleNotFound(?EmbeddedMetadata $parentPropertyMetadata, string $resourceClass): EmbeddedMetadata
    {
        if (null !== $parentPropertyMetadata) {
            return $parentPropertyMetadata;
        }

        throw new ResourceClassNotFoundException(sprintf('Resource "%s" not found.', $resourceClass));
    }

    private function createMetadata(ApiEmbedded $annotation, EmbeddedMetadata $parentResourceMetadata = null): EmbeddedMetadata
    {
        if (!$parentResourceMetadata) {
            $obj = new EmbeddedMetadata();
            $obj->setShortName($annotation->shortName);
            return $obj;
        }

        $resourceMetadata = $parentResourceMetadata;
        if(!$resourceMetadata->getShortName()){
            $resourceMetadata->setShortName($annotation->shortName);
        }

        return $resourceMetadata;
    }
}
