<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

/**
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
interface EmbeddedNameCollectionFactoryInterface
{
    /**
     * @return string[]
     */
    public function create(): array;
}
