<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata;

/**
 * Creates an embedded metadata value object.
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
interface EmbeddedMetadataFactoryInterface
{
    /**
     * Creates an embedded metadata.
     *
     * @throws ResourceClassNotFoundException
     */
    public function create(string $embeddedClass): EmbeddedMetadata;
}
