<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory;

use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata;

/**
 * Sets default values for embedded metadata if not set already
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final readonly class DefaultsEmbeddedMetadataFactory implements EmbeddedMetadataFactoryInterface
{
    public function __construct(private EmbeddedMetadataFactoryInterface $decorated)
    {
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function create(string $embeddedClass): EmbeddedMetadata
    {
        $embeddedMetadata = $this->decorated->create($embeddedClass);

        if (null !== $embeddedMetadata->getShortName()) {
            return $embeddedMetadata;
        }

        if (false !== $pos = strrpos($embeddedClass, '\\')) {
            return $embeddedMetadata->setShortName(substr($embeddedClass, $pos + 1));
        }

        return $embeddedMetadata->setShortName($embeddedClass);
    }
}
