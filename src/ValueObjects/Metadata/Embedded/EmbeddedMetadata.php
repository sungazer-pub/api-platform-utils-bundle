<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded;


class EmbeddedMetadata
{
    private ?string $shortName = null;

    /**
     * @return string
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName(?string $shortName): EmbeddedMetadata
    {
        $this->shortName = $shortName;
        return $this;
    }

}