<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Annotation;


use Attribute;

/**
 * ApiEmbedded annotation, for Structured Values.
 *
 * @author Luca Nardelli <luca@sungazer.io>
 *
 * @Annotation
 * @Target({"CLASS"})
 * @Attributes(
 *     @Attribute("shortName", type="string"),
 * )
 */
#[Attribute(Attribute::TARGET_CLASS)]
final class ApiEmbedded
{
    /**
     * @var string
     */
    public $shortName;

}
