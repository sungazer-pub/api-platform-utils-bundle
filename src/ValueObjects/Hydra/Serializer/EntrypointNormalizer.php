<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Hydra\Serializer;

use ApiPlatform\Core\Hydra\Serializer\EntrypointNormalizer as ApiEntrypointNormalizer;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedMetadataFactoryInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedNameCollectionFactoryInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Decorates Api Platform's Hydra entrypoint normalizer to add support for non-resource objects
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final readonly class EntrypointNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    public const FORMAT = 'jsonld';

    public function __construct(private ApiEntrypointNormalizer $decorated, private EmbeddedNameCollectionFactoryInterface $embeddedNameCollectionFactory, private EmbeddedMetadataFactoryInterface $embeddedMetadataFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function normalize($object, string $format = null, array $context = []): float|int|bool|\ArrayObject|array|string|null
    {
        $data = $this->decorated->normalize($object, $format, $context);
        foreach ($this->embeddedNameCollectionFactory->create() as $embedded) {
            $metadata = $this->embeddedMetadataFactory->create($embedded);
            // Dummy IRI to make the generator happy
            $data[lcfirst((string) $metadata->getShortName())] = '/api/dummy_iri';
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $this->decorated->supportsNormalization($data, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
