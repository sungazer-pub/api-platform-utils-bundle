<?php

/*
 * File inspired by API Platform
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Hydra\Serializer;

use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Property\PropertyMetadata;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use ApiPlatform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface;
use ApiPlatform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use DateTimeInterface;
use Exception;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\EmbeddedMetadata;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedMetadataFactoryInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ValueObjects\Metadata\Embedded\Factory\EmbeddedNameCollectionFactoryInterface;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Decorates Api Platform's Hydra documentation normalizer to add support for non-resource objects
 *
 * @author Luca Nardelli <luca.nardelli@protonmail.com>
 */
final readonly class DocumentationNormalizer implements NormalizerInterface
{

    public const FORMAT = 'jsonld';

    public function __construct(
        private NormalizerInterface                                                 $decorated,
        private ResourceNameCollectionFactoryInterface                              $resourceNameCollectionFactory,
        private PropertyMetadataFactoryInterface                                    $propertyMetadataFactory,
        private PropertyNameCollectionFactoryInterface                              $propertyNameCollectionFactory,
        private EmbeddedNameCollectionFactoryInterface                              $embeddedNameCollectionFactory,
        private EmbeddedMetadataFactoryInterface                                    $embeddedMetadataFactory,
        private ?\Symfony\Component\Serializer\NameConverter\NameConverterInterface $nameConverter = null,
    )
    {
    }

    public function getSupportedTypes(?string $format): array
    {
        return $this->decorated->getSupportedTypes($format);
    }

    #[\Override]
    public function supportsNormalization(mixed $data, ?string $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    #[\Override]
    public function normalize($object,
                              string $format = null,
                              array $context = []): float|int|bool|\ArrayObject|array|string|null
    {
        $data = $this->decorated->normalize($object, $format, $context);

        // Find indexes for classes (needed later)
        $docClasses = [];
        foreach ($data['hydra:supportedClass'] as $idx => $class) {
            $docClasses[$class['@id']] = $idx;
        }

        $shortNameToResourceMapping = [];
        $shortNameToEmbeddedMapping = [];

        // Generate resource mappings (needed later)
        foreach ($this->resourceNameCollectionFactory->create() as $resourceName) {
            $metadata                                              = $this->resourceMetadataFactory->create(
                $resourceName
            );
            $shortNameToResourceMapping[$metadata->getShortName()] = $resourceName;
        }

        // Generate new resources for each Embedded
        foreach ($this->embeddedNameCollectionFactory->create() as $embeddedName) {
            $metadata = $this->embeddedMetadataFactory->create($embeddedName);
            // Generate mapping for embedded as well
            $shortNameToEmbeddedMapping[$metadata->getShortName()] = $embeddedName;

            $shortName                      = $metadata->getShortName();
            $prefixedShortName              = "#$shortName";
            $classData                      = $this->getClass(
                $embeddedName,
                $metadata,
                $shortName,
                $prefixedShortName,
                $context
            );
            $data['hydra:supportedClass'][] = $classData;
            // Patch entrypoint properties
            $this->populateEntrypointProperties(
                $classData['rdfs:label'],
                $classData['@id'],
                $data['hydra:supportedClass'][$docClasses['#Entrypoint']]['hydra:supportedProperty']
            );
        }

        // Patch all supported classes where range was not set

        foreach ($data['hydra:supportedClass'] as $supportedClass) {
            $classShortName = $supportedClass['hydra:title'];
            $classId        = $supportedClass['@id'];
            $class          = $shortNameToResourceMapping[$classShortName] ?? $shortNameToEmbeddedMapping[$classShortName] ?? null;
            // Skip anything that is not a resource or an embedded (e.g. Entrypoint)
            if (!$class) {
                continue;
            }
            foreach ($supportedClass['hydra:supportedProperty'] as $idx => $supportedProperty) {
                $propName = $supportedProperty['hydra:title'];
                // If property does not have a range, it is a candidate for patching
                if (!array_key_exists('range', $supportedProperty['hydra:property'])) {
                    $prop = $this->propertyMetadataFactory->create($class, $propName);
                    if ($prop->getType() && $prop->getType()->getBuiltinType() === 'object') {
                        $embeddedClass = $prop->getType()->getClassName();
                        // Skip on error
                        try {
                            $metadata = $this->embeddedMetadataFactory->create($embeddedClass);
                        } catch (Exception) {
                            continue;
                        }
                        // Patch documentation
                        $data['hydra:supportedClass'][$docClasses[$classId]]
                        ['hydra:supportedProperty'][$idx]['hydra:property']['range']              = "#" . $metadata->getShortName(
                            );
                        $data['hydra:supportedClass'][$docClasses[$classId]]
                        ['hydra:supportedProperty'][$idx]['hydra:property']['owl:maxCardinality'] = 1;
                    } else if ($prop->getType() && $prop->getType()->getBuiltinType() === 'array') {
                        $collectionValueType = $prop->getType()->getCollectionValueType();
                        if (!$collectionValueType) {
                            continue;
                        }
                        if ($collectionValueType->getBuiltinType() === 'object') {
                            $embeddedClass = $collectionValueType->getClassName();
                            try {
                                $metadata = $this->embeddedMetadataFactory->create($embeddedClass);
                            } catch (Exception) {
                                continue;
                            }
                            $range = "#" . $metadata->getShortName();
                        } else {
                            $range = $this->getRangeFromBuiltinType($collectionValueType);
                        }

                        // Patch documentation
                        $data['hydra:supportedClass'][$docClasses[$classId]]
                        ['hydra:supportedProperty'][$idx]['hydra:property']['range'] = $range;
                    }
                }

            }
        }

//        $context['_valueObjects'] = [];
//        foreach ($this->resourceNameCollectionFactory->create() as $resourceClass) {
//            $resourceMetadata  = $this->resourceMetadataFactory->create($resourceClass);
//            $shortName         = $resourceMetadata->getShortName();
//            $prefixedShortName = $resourceMetadata->getIri() ?? "#$shortName";
//            $docClass          = $data['hydra:supportedClass'][$docClasses[$prefixedShortName]];
//
//            foreach ($docClass['hydra:supportedProperty'] as $idx => $docProperty) {
//                if (!array_key_exists('range', $docProperty['hydra:property'])) {
//                    $normalizedPropertyName = $docProperty['hydra:property']['rdfs:label'];
//                    $prop                   = $this->propertyMetadataFactory->create($resourceClass, $normalizedPropertyName);
//                    if ($prop->getType() && $prop->getType()->getBuiltinType() === 'object') {
//                        $newClass                                                                 = $prop->getType()->getClassName();
//                        $exploded                                                                 = explode("\\", $newClass);
//                        $newShortName                                                             = end($exploded);
//                        $newPrefixedShortName                                                     = "#$newShortName";
//                        $newClassData                                                             = $context['_valueObjects'][$newPrefixedShortName] ?? $this->getClass($newClass,
//                                $resourceMetadata,
//                                $newShortName,
//                                $newPrefixedShortName,
//                                $context);
//                        $data['hydra:supportedClass'][$docClasses[$prefixedShortName]]
//                        ['hydra:supportedProperty'][$idx]['hydra:property']['range']              = $newPrefixedShortName;
//                        $data['hydra:supportedClass'][$docClasses[$prefixedShortName]]
//                        ['hydra:supportedProperty'][$idx]['hydra:property']['owl:maxCardinality'] = 1;
//                        $context['_valueObjects'][$newClassData['@id']]                           = $newClassData;
//                    }
//                }
//            }
//        }
//        foreach ($context['_valueObjects'] as $valueObject) {
//            $data['hydra:supportedClass'][] = $valueObject;
//            $this->populateEntrypointProperties($valueObject['rdfs:label'], $valueObject['@id'], $data['hydra:supportedClass'][$docClasses['#Entrypoint']]['hydra:supportedProperty']);
//        }
        return $data;
    }

    /**
     * Gets a Hydra class.
     */
    private function getClass(string           $resourceClass,
                              EmbeddedMetadata $embeddedMetadata,
                              string           $shortName,
                              string           $prefixedShortName,
                              array            $context): array
    {
        $class = [
            '@id'                      => $prefixedShortName,
            '@type'                    => 'hydra:Class',
            'rdfs:label'               => $shortName,
            'hydra:title'              => $shortName,
            'hydra:supportedProperty'  => $this->getHydraProperties(
                $resourceClass,
                $embeddedMetadata,
                $shortName,
                $prefixedShortName,
                $context
            ),
            'hydra:supportedOperation' => [],
        ];
//
//        if (null !== $description = $resourceMetadata->getDescription()) {
//            $class['hydra:description'] = $description;
//        }
//
//        if ($resourceMetadata->getAttribute('deprecation_reason')) {
//            $class['owl:deprecated'] = true;
//        }

        return $class;
    }

    /**
     * Gets Hydra properties.
     */
    private function getHydraProperties(string           $resourceClass,
                                        EmbeddedMetadata $embeddedMetadata,
                                        string           $shortName,
                                        string           $prefixedShortName,
                                        array            $context): array
    {
        $properties = [];
        foreach ($this->propertyNameCollectionFactory->create($resourceClass, []) as $propertyName) {
            $propertyMetadata = $this->propertyMetadataFactory->create($resourceClass, $propertyName);
            if (true === $propertyMetadata->isIdentifier() && false === $propertyMetadata->isWritable()) {
                continue;
            }

            if ($this->nameConverter) {
                $propertyName = $this->nameConverter->normalize($propertyName, $resourceClass, self::FORMAT, $context);
            }

            $properties[] = $this->getProperty($propertyMetadata, $propertyName, $prefixedShortName, $shortName);
        }

        return $properties;
    }

    /**
     * Gets a property definition.
     */
    private function getProperty(PropertyMetadata $propertyMetadata,
                                 string           $propertyName,
                                 string           $prefixedShortName,
                                 string           $shortName): array
    {
        $propertyData = [
            '@id'        => $propertyMetadata->getIri() ?? "#$shortName/$propertyName",
            '@type'      => false === $propertyMetadata->isReadableLink() ? 'hydra:Link' : 'rdf:Property',
            'rdfs:label' => $propertyName,
            'domain'     => $prefixedShortName,
        ];

        $type = $propertyMetadata->getType();

        if (null !== $type && !$type->isCollection() && (null !== $className = $type->getClassName())) {
            $propertyData['owl:maxCardinality'] = 1;
        }

        $property = [
            '@type'          => 'hydra:SupportedProperty',
            'hydra:property' => $propertyData,
            'hydra:title'    => $propertyName,
            'hydra:required' => $propertyMetadata->isRequired(),
            'hydra:readable' => $propertyMetadata->isReadable(),
            'hydra:writable' => $propertyMetadata->isWritable() || $propertyMetadata->isInitializable(),
        ];
        if (null !== $range = $this->getRange($propertyMetadata)) {
            $property['hydra:property']['range'] = $range;
        }

        if (null !== $description = $propertyMetadata->getDescription()) {
            $property['hydra:description'] = $description;
        }

        if ($propertyMetadata->getAttribute('deprecation_reason')) {
            $property['owl:deprecated'] = true;
        }

        return $property;
    }

    /**
     * Gets the range of the property.
     */
    private function getRange(PropertyMetadata $propertyMetadata): ?string
    {
        $jsonldContext = $propertyMetadata->getAttributes()['jsonld_context'] ?? [];

        if (isset($jsonldContext['@type'])) {
            return $jsonldContext['@type'];
        }

        if (null === $type = $propertyMetadata->getType()) {
            return null;
        }

        if ($type->isCollection() && null !== $collectionType = $type->getCollectionValueType()) {
            $type = $collectionType;
        }

        return $this->getRangeFromBuiltinType($type);
    }

    private function getRangeFromBuiltinType(Type $type): ?string
    {
        switch ($type->getBuiltinType()) {
            case Type::BUILTIN_TYPE_STRING:
                return 'xmls:string';
            case Type::BUILTIN_TYPE_INT:
                return 'xmls:integer';
            case Type::BUILTIN_TYPE_FLOAT:
                return 'xmls:decimal';
            case Type::BUILTIN_TYPE_BOOL:
                return 'xmls:boolean';
            case Type::BUILTIN_TYPE_OBJECT:
                if (null === $className = $type->getClassName()) {
                    return null;
                }
                if (is_a($className, DateTimeInterface::class, true)) {
                    return 'xmls:dateTime';
                }
                break;
        }

        return null;
    }

    /**
     * Populates entrypoint properties.
     */
    private function populateEntrypointProperties(string $shortName,
                                                  string $prefixedShortName,
                                                  array  &$entrypointProperties): void
    {
        $entrypointProperty = [
            '@type'          => 'hydra:SupportedProperty',
            'hydra:property' => [
                '@id'                      => sprintf('#Entrypoint/%s', lcfirst($shortName)),
                '@type'                    => 'hydra:Link',
                'domain'                   => '#Entrypoint',
                'rdfs:label'               => "The collection of $shortName resources",
                'rdfs:range'               => [
                    ['@id' => 'hydra:Collection'],
                    [
                        'owl:equivalentClass' => [
                            'owl:onProperty'    => ['@id' => 'hydra:member'],
                            'owl:allValuesFrom' => ['@id' => $prefixedShortName],
                        ],
                    ],
                ],
                'hydra:supportedOperation' => [],
            ],
            'hydra:title'    => "The collection of $shortName resources",
            'hydra:readable' => true,
            'hydra:writable' => false,
        ];

        $entrypointProperties[] = $entrypointProperty;
    }

    /**
     * Gets the context for the property name factory.
     */
    private function getPropertyNameCollectionFactoryContext(ResourceMetadata $resourceMetadata): array
    {
        $attributes = $resourceMetadata->getAttributes();
        $context    = [];

        if (isset($attributes['normalization_context'][AbstractNormalizer::GROUPS])) {
            $context['serializer_groups'] = (array)$attributes['normalization_context'][AbstractNormalizer::GROUPS];
        }

        if (!isset($attributes['denormalization_context'][AbstractNormalizer::GROUPS])) {
            return $context;
        }

        if (isset($context['serializer_groups'])) {
            foreach ((array)$attributes['denormalization_context'][AbstractNormalizer::GROUPS] as $groupName) {
                $context['serializer_groups'][] = $groupName;
            }

            return $context;
        }

        $context['serializer_groups'] = (array)$attributes['denormalization_context'][AbstractNormalizer::GROUPS];

        return $context;
    }
}
