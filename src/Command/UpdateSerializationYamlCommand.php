<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Command;


use PhpParser\Node;
use PhpParser\NodeFinder;
use PhpParser\ParserFactory;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\NodeTraverser;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

#[\Symfony\Component\Console\Attribute\AsCommand('sg:update-serialization-yaml')]
class UpdateSerializationYamlCommand extends Command
{
    #[\Override]
    protected function configure(): void
    {
        $this->addOption('directory', 'd', InputOption::VALUE_OPTIONAL + InputOption::VALUE_IS_ARRAY, "List of directories where to look for entities", ['src/Entity'])
            ->addArgument('outputDirectory', InputArgument::OPTIONAL, "Output directory", "config/serializer")
            ->setDescription('Updates serialization YAML files from entity definitions');
        parent::configure();
    }

    #[\Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set("memory_limit", "4G");

        $io = new SymfonyStyle($input, $output);

        $directories = $input->getOption('directory');
        $outputDir   = $input->getArgument('outputDirectory');

        $filesystem = new Filesystem();

        if (!$filesystem->exists($outputDir)) {
            $filesystem->mkdir($outputDir);
        }

        $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);

        $finder = new  Finder();
        $finder->files()->in($directories)->name("*.php");
        foreach ($finder as $file) {
//            $io->writeln("Processing " . $file->getFilename());
            $nameResolver  = new NameResolver();
            $nodeTraverser = new NodeTraverser();
            $nodeTraverser->addVisitor($nameResolver);
//            $entityPath = dirname("Entity/" . explode("Entity/", $file->getRealPath())[1]);
            $statements = $parser->parse(file_get_contents($file->getRealPath()));
            $statements = $nodeTraverser->traverse($statements);
            $nodeFinder = new NodeFinder();
            $classes    = $nodeFinder->findInstanceOf($statements, Node\Stmt\ClassLike::class);
            /** @var Node\Stmt\ClassLike $class */
            foreach ($classes as $class) {
                $name   = $class->name;
                $fqName = $class->namespacedName->toString();
//                $io->writeln("Processing " . $class->namespacedName);
                $yamlFile = "$outputDir/".str_replace(['App\\','\\'],['','/'],$fqName).".yaml";
                $def      = [];
                if ($filesystem->exists($yamlFile)) {
                    $def = Yaml::parseFile($yamlFile);
                }

                $def[$fqName] ??= [];
                $def[$fqName]['attributes'] ??= [];

                // Get all class properties
                $refClass   = new ReflectionClass($fqName);
                $properties = $refClass->getProperties();
                // Do yaml parsing here
                foreach ($properties as $property) {
                    if ($property->isStatic()) {
                        continue;
                    }
                    $pName                              = $property->getName();
                    $def[$fqName] ??= [];
                    $def[$fqName]['attributes'][$pName] ??= ['groups' => []];
                }

                if (!file_exists(dirname($yamlFile))) {
                    mkdir(dirname($yamlFile), 0777, true);
                }
                file_put_contents($yamlFile, Yaml::dump($def, 100, 4, Yaml::DUMP_OBJECT));
            }
        }


        $io->success("Done!");
        return 0;
    }

}
