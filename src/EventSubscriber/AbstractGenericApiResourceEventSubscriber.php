<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\EventListener\WriteListener;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use ApiPlatform\Core\Validator\EventListener\ValidateListener;
use Doctrine\Common\Util\ClassUtils;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

abstract class AbstractGenericApiResourceEventSubscriber implements EventSubscriberInterface
{
    public function __construct(protected \Psr\Log\LoggerInterface $logger, protected \ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface $resourceMetadataFactory)
    {
    }

    /**
     * @inheritDoc
     */
    #[\Override]
    public static function getSubscribedEvents()
    {
        $events = [
        ];
        if (!empty(static::getPostValidateSubscribedResources())) {
            $events[KernelEvents::VIEW][] = ['postValidate', EventPriorities::POST_VALIDATE];
        }
        if (!empty(static::getPreWriteSubscribedResources())) {
            $events[KernelEvents::VIEW][] = ['preWrite', EventPriorities::PRE_WRITE];
        }
        if (!empty(static::getPostWriteSubscribedResources())) {
            $events[KernelEvents::VIEW][] = ['postWrite', EventPriorities::POST_WRITE];
        }
        return $events;
    }

    public static function getPostValidateSubscribedResources(): array
    {
        return [];
    }

    public static function getPreWriteSubscribedResources(): array
    {
        return [];
    }

    public static function getPostWriteSubscribedResources(): array
    {
        return [];
    }

    public function postValidate(ViewEvent $event): void
    {
        if (!$this->validateListenerExecutes($event)) {
            return;
        }

        $this->dispatchToClassListener($event, static::getPostValidateSubscribedResources());
    }

    private function validateListenerExecutes(ViewEvent $event): bool
    {
        $controllerResult = $event->getControllerResult();
        $request          = $event->getRequest();

        if (
            $controllerResult instanceof Response
            || $request->isMethodSafe()
            || $request->isMethod('DELETE')
            || !($attributes = RequestAttributesExtractor::extractAttributes($request))
            || !$attributes['receive']
            || $this->isOperationAttributeDisabled($attributes, ValidateListener::OPERATION_ATTRIBUTE_KEY)
        ) {
            return false;
        }
        return true;
    }

    private function dispatchToClassListener(ViewEvent $event, array $listeners)
    {
        $entity = $event->getControllerResult();
        if (!is_object($entity)) {
            return $entity;
        }
        // See https://github.com/doctrine/common/issues/867 for deprecation warning
        $resourceClass = ClassUtils::getRealClass($entity::class);
        // POST = Add to collection, PUT = update entity, DELETE = delete

        if ($subListeners = $listeners[$resourceClass] ?? null) {
            foreach ($subListeners as $subListener) {
                if (!method_exists($this, $subListener)) {
                    throw new Exception("method ${subListener} not existing");
                }
                $this->logger->debug("Calling $subListener for entity $resourceClass");
                $entity = call_user_func([$this, $subListener], $entity ?: $event->getControllerResult(), $event);
                if ($entity !== null) {
                    $event->setControllerResult($entity);
                }
            }
        }

        return $entity;
    }

    private function isOperationAttributeDisabled(array $attributes, string $attribute, bool $default = false, bool $resourceFallback = true): bool
    {
        if (null === $this->resourceMetadataFactory) {
            return $default;
        }

        $resourceMetadata = $this->resourceMetadataFactory->create($attributes['resource_class']);

        return !((bool)$resourceMetadata->getOperationAttribute($attributes, $attribute, !$default, $resourceFallback));
    }

    public function postWrite(ViewEvent $event): void
    {
        if (!$this->writeListenerExecutes($event)) {
            return;
        }

        $this->dispatchToClassListener($event, static::getPostWriteSubscribedResources());
    }

    private function writeListenerExecutes(ViewEvent $event): bool
    {
        $controllerResult = $event->getControllerResult();
        $request          = $event->getRequest();

        if (
            $controllerResult instanceof Response
            || $request->isMethodSafe()
            || !($attributes = RequestAttributesExtractor::extractAttributes($request))
            || !$attributes['persist']
            || $this->isOperationAttributeDisabled($attributes, WriteListener::OPERATION_ATTRIBUTE_KEY)
        ) {
            return false;
        }
        return true;
    }

    public function preWrite(ViewEvent $event): void
    {
        if (!$this->writeListenerExecutes($event)) {
            return;
        }

        $this->dispatchToClassListener($event, static::getPreWriteSubscribedResources());
    }

}
