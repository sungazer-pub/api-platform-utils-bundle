<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiSerializationContext\Annotation;


use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\ORM\Mapping\Annotation;

/**
 * @Annotation()
 * @Target("CLASS")
 */
class ApiSerializationContext implements Annotation
{
    /**
     * @var array = [
     *     'expr' => 'symfony expression',
     *     'normalization_context' => ['groups' => []],
     *     'denormalization_context' => ['groups' => []],
     * ]
     */
    public $normalizationStage;
    /**
     * @var array = [
     *     'expr' => 'symfony expression',
     *     'normalization_context' => ['groups' => []],
     *     'denormalization_context' => ['groups' => []],
     * ]
     */
    public $requestStage;
    // TODO Implement below
    /**
     * @var array = [
     *     'operation_name' => [
     *       'expr' => 'symfony expression',
     *       'normalization_context' => ['groups' => []],
     *       'denormalization_context' => ['groups' => []],
     *     ]
     * ]
     */
    //public $collectionOperations;
    /**
     * @var array = [
     *     'operation_name' => [
     *       'expr' => 'symfony expression',
     *       'normalization_context' => ['groups' => []],
     *       'denormalization_context' => ['groups' => []],
     *     ]
     * ]
     */
    //public $itemOperations;
}