<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiSerializationContext;

use ApiPlatform\Serializer\SerializerContextBuilderInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ApiSerializationContext\Annotation\ApiSerializationContext;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Services\GenericClassMetadataExtractor;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final readonly class ApiSerializationContextBuilder implements SerializerContextBuilderInterface
{
  public function __construct(
    private SerializerContextBuilderInterface    $decorated,
    private ExpressionLanguage                   $expressionLanguage,
    private AuthenticationTrustResolverInterface $authenticationTrustResolver,
    private AuthorizationCheckerInterface        $authorizationChecker,
    private GenericClassMetadataExtractor                    $extractor,
    private TokenStorageInterface                $tokenStorageInterface,
  )
  {
  }

  #[\Override]
  public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
  {
    $context       = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
    $resourceClass = $context['resource_class'] ?? null;

    $token = $this->tokenStorageInterface->getToken();
    if (!$token) {
      return $context;
    }

    $metadataArray = $this->extractor->extract($resourceClass, ApiSerializationContext::class);
    if ($metadataArray) {
      foreach ($metadataArray as $metadata) {
        // If the annotation does not have metadata for this stage, skip
        if (!$metadata->requestStage) {
          continue;
        }
        $ctx = $metadata->requestStage;
        //TODO Per operation override
        $result = (bool)$this->expressionLanguage->evaluate($ctx['expr'], [
          'token'          => $token,
          'user'           => $token->getUser(),
          'trust_resolver' => $this->authenticationTrustResolver,
          // needed for the is_granted expression function
          'auth_checker'   => $this->authorizationChecker,
        ]);
        if ($result) {
          if ($normalization && array_key_exists('groups', $ctx['normalization_context'] ?? [])) {
            $context['groups'] = array_merge($context['groups'], $ctx['normalization_context']['groups']);
          } else if (!$normalization && array_key_exists('groups', $ctx['denormalization_context'] ?? [])) {
            $context['groups'] = array_merge($context['groups'], $ctx['denormalization_context']['groups']);
          }
        }
      }
    }


    return $context;
  }
}
