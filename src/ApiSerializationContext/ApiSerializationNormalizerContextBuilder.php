<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiSerializationContext;

use Sungazer\Bundle\ApiPlatformUtilsBundle\ApiSerializationContext\Annotation\ApiSerializationContext;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Services\GenericClassMetadataExtractor;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class ApiSerializationNormalizerContextBuilder
  implements NormalizerInterface, NormalizerAwareInterface, DenormalizerInterface, DenormalizerAwareInterface
{
  use NormalizerAwareTrait;
  use DenormalizerAwareTrait;

  private const NORM_ALREADY_CALLED = 'api_serialization_normalizer_already_called';
  private const DENORM_ALREADY_CALLED = 'api_serialization_denormalizer_already_called';
  private const METADATA_KEY = 'api_serialization_annotations';

  public function __construct(
    private ExpressionLanguage                   $expressionLanguage,
    private AuthenticationTrustResolverInterface $authenticationTrustResolver,
    private AuthorizationCheckerInterface        $authorizationChecker,
    private GenericClassMetadataExtractor                    $extractor,
    private TokenStorageInterface                $tokenStorageInterface)
  {
  }

  public function getSupportedTypes(?string $format): array
  {
    return array_merge_recursive($this->normalizer->getSupportedTypes($format), $this->denormalizer->getSupportedTypes($format));
  }


  #[\Override]
  public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
  {
    if (isset($context[self::DENORM_ALREADY_CALLED])) {
      return false;
    }

    /**
     * type can be something like 'string', or a FQCN (App\Entity\Class) or an array (App\Entity\Class[])
     * If type is a FQCN we can proceed, but if it is an array we need to ignore (as this will be called again for the item later)
     */
    if (!class_exists($type)) {
      return false;
    }

    return (bool)$this->extractor->extract($type, ApiSerializationContext::class);
  }

  #[\Override]
  public function denormalize($data, string $type, string $format = null, array $context = []): mixed
  {
    $metadataArray = $this->extractor->extract($type, ApiSerializationContext::class);

    $this->processMetadataArray($metadataArray, $context, false);

    $context[self::DENORM_ALREADY_CALLED] = true;
    return $this->denormalizer->denormalize($data, $type, $format, $context);
  }

  /**
   * @param ApiSerializationContext[] $metadataArray
   */
  private function processMetadataArray(array $metadataArray,
                                        array &$context,
                                        bool  $normalization,
                                              $object = null): void
  {
    $token = $this->tokenStorageInterface->getToken();
    if (!$token) {
      return;
    }
    foreach ($metadataArray as $metadata) {
      // If the annotation does not have metadata for this stage, skip
      if (!$metadata->normalizationStage) {
        continue;
      }
      $ctx = $metadata->normalizationStage;
      //TODO Per operation override
      $result = (bool)$this->expressionLanguage->evaluate($ctx['expr'], [
        'token'          => $token,
        'user'           => $token->getUser(),
        'trust_resolver' => $this->authenticationTrustResolver,
        'object'         => $normalization ? $object : $context[AbstractNormalizer::OBJECT_TO_POPULATE] ?? null,
        // needed for the is_granted expression function
        'auth_checker'   => $this->authorizationChecker,
      ]);

      if ($result) {
        if ($normalization && array_key_exists('groups', $ctx['normalization_context'] ?? [])) {
          $context['groups'] = array_merge($context['groups'], $ctx['normalization_context']['groups']);
        } else if (!$normalization && array_key_exists('groups', $ctx['denormalization_context'] ?? [])) {
          $context['groups'] = array_merge($context['groups'], $ctx['denormalization_context']['groups']);
        }
      }
    }
  }

  #[\Override]
  public function supportsNormalization($data, string $format = null, array $context = []): bool
  {
    if (isset($context[self::NORM_ALREADY_CALLED]) || gettype($data) !== 'object') {
      return false;
    }

    return (bool)$this->extractor->extract($data::class, ApiSerializationContext::class);
  }

  #[\Override]
  public function normalize($object,
                            string $format = null,
                            array $context = []): float|array|\ArrayObject|bool|int|string|null
  {
    $metadataArray = $this->extractor->extract($object::class, ApiSerializationContext::class);

    $this->processMetadataArray($metadataArray, $context, true, $object);

    $context[self::NORM_ALREADY_CALLED] = true;
    return $this->normalizer->normalize($object, $format, $context);
  }
}
