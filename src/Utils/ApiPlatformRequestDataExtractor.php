<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Utils;


use Symfony\Component\HttpFoundation\Request;

class ApiPlatformRequestDataExtractor
{
    public static function getPreviousData(Request $request){
        return $request->attributes->get('previous_data');
    }

}