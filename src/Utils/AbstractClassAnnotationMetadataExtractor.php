<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Utils;


use Doctrine\Common\Annotations\Reader;
use Psr\Cache\CacheItemPoolInterface;
use ReflectionClass;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Cache\CachedTrait;


abstract class AbstractClassAnnotationMetadataExtractor
{
    use CachedTrait;

    public function __construct(private string $cacheKey, private string $annotationClass, private Reader $reader, CacheItemPoolInterface $cacheItemPool)
    {
        $this->cacheItemPool   = $cacheItemPool;
    }

    /**
     * @param $className
     */
    public function extract($className): array
    {
        return $this->getCached(sprintf("%s.%s", $this->cacheKey, md5((string) $className)), function () use ($className): array {
            $refClass    = new ReflectionClass($className);
            // Try reading attribute first, then annotation
            $attributes = $refClass->getAttributes($this->annotationClass);
            if(count($attributes) > 0){
                return array_map(fn(\ReflectionAttribute $attr): object => $attr->newInstance(), $attributes);
            }
            $annotations = $this->reader->getClassAnnotations($refClass);
            if (!$annotations) {
                return [];
            }
            // Filter annotations that are not ours
            $annotations = array_filter($annotations, fn($ann): bool => $ann::class === $this->annotationClass);
            return $annotations;
        });
    }

}
