<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Doctrine\EntityListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\UnableToDeleteFile;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\BaseFile;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\SoftDeletableBaseFileInterface;
use Throwable;

class BaseFileEntityListener
{
    /**
     * @var array = [
     *     ['path' => 'string']
     * ]
     */
    private $toDelete = [];

    public function __construct(private readonly LoggerInterface $logger, private readonly FilesystemOperator $storage)
    {
    }

    #[ORM\PreRemove]
    public function preRemove(BaseFile $entity, LifecycleEventArgs $args): void
    {
        $this->toDelete[] = ['path' => $entity->getPath()];
    }

    #[ORM\PostPersist]
    #[ORM\PostUpdate]
    public function handleSoftDelete(BaseFile $entity, LifecycleEventArgs $args): void{
        if($entity instanceof SoftDeletableBaseFileInterface){
            if($entity->isDeleted()){
                $this->cleanupBaseFile(['path' => $entity->getPath()]);
            }
        }
    }

    #[ORM\PostRemove]
    public function postRemove(BaseFile $entity, LifecycleEventArgs $args): void
    {
        // Delete remote file
        foreach ($this->toDelete as $toDelete) {
            $this->cleanupBaseFile($toDelete);
        }
        $this->toDelete = [];
    }

    /**
     * @param array $data = [
     *     'path' => 'path string'
     * ]
     */
    private function cleanupBaseFile(array $data): void{
        $path = $data['path'] ?? null;
        // No object to delete if we don't have a path!
        if(!$path){
            return;
        }
        $this->logger->info("Deleting file at path {$path}");
        try {
            $this->storage->delete($path);
        } catch (UnableToDeleteFile $e) {
            $this->logger->warning("Could not delete file {$path}: {$e->getMessage()}");
        } catch (Throwable) {
            $this->logger->warning("Could not delete file {$path}");
        }
    }

}
