<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Doctrine\EntityListener;


use DateTime;
use Doctrine\ORM\Event\PostLoadEventArgs;
use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\BaseFile;

class BaseFilePublicUrlEntityListener
{
    public function __construct(private readonly LoggerInterface $logger, private readonly FilesystemOperator $storage)
    {
    }

    public function postLoad(BaseFile $entity, PostLoadEventArgs $args): void
    {
        if ($entity instanceof BaseFile) {
            // No url to generate if we don't have a path!
            if (!$entity->getPath()) {
                return;
            }
            $now = DateTime::createFromFormat('U.u', sprintf('%.6F', microtime(true)));
            // Regenerate url whenever we are 1 day until expiry
            if ($entity->getUrlExpiresAt() !== null) {
                $diff = $entity->getUrlExpiresAt()->getTimestamp() - $now->getTimestamp();

                if ($diff >= 24 * 3600) {
                    return;
                }
            }
            $expiry = $now->modify('+1 week');
            $url    = $this->storage->temporaryUrl($entity->getPath(), $expiry);
            $entity->setUrl($url)
                   ->setUrlExpiresAt($expiry);

            $this->logger->debug("Updating presigned url for file", ['file' => $entity->getId()]);
            $args->getObjectManager()->createQueryBuilder()
                 ->update($entity::class, 'o')
                 ->set('o.url', ':url')
                 ->set('o.urlExpiresAt', ':urlExpiresAt')
                 ->where('o.id = :id')
                 ->setParameter('url', $entity->getUrl())
                 ->setParameter('urlExpiresAt', $entity->getUrlExpiresAt())
                 ->setParameter('id', $entity->getId())
                 ->getQuery()->execute();
//                $args->getEntityManager()->flush($entity);

//            $adapter = $this->storage->getAdapter();
//            if ($adapter instanceof AwsS3Adapter) {
//                $client = $adapter->getClient();
//                $bucket = $adapter->getBucket();
//                //Creating a presigned URL
//                $cmd = $client->getCommand('GetObject', [
//                    'Bucket' => $bucket,
//                    'Key'    => $entity->getPath()
//                ]);
//
//                $request = $client->createPresignedRequest($cmd, '+1 week');
//                $entity->setUrl((string)$request->getUri())
//                    ->setUrlExpiresAt($now->modify('+1 week'));
//                $this->logger->debug("Updating presigned url for file", ['file' => $entity->getId()]);
//                $args->getEntityManager()->createQueryBuilder()
//                    ->update(get_class($entity),'o')
//                    ->set('o.url',':url')
//                    ->set('o.urlExpiresAt',':urlExpiresAt')
//                    ->where('o.id = :id')
//                    ->setParameter('url' , $entity->getUrl())
//                    ->setParameter('urlExpiresAt' , $entity->getUrlExpiresAt())
//                    ->setParameter('id', $entity->getId())
//                    ->getQuery()->execute();
////                $args->getEntityManager()->flush($entity);
//            }
        }
    }

}
