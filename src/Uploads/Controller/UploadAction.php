<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Controller;


use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\BaseFile;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Utils\UploadHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
class UploadAction extends AbstractController
{

    public function __construct()
    {
    }

    public function __invoke(Request $request)
    {
        $objectClass = $request->attributes->get('_api_resource_class');

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        UploadHelper::setContextFromRequest($request, [
            'files' => [
                'file' => $uploadedFile
            ]
        ]);

        /** @var BaseFile $mediaObject */
        $mediaObject = new $objectClass();

        return $mediaObject;
    }
}
