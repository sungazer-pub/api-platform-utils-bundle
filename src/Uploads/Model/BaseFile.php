<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class File
 */
#[ORM\MappedSuperclass]
abstract class BaseFile
{
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['base_file:read'])]
    protected $path;
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['base_file:read'])]
    protected $name;
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['base_file:read'])]
    protected $url;
    /**
     * @var DateTime
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['base_file:read'])]
    protected $urlExpiresAt;

    public function __construct()
    {
    }

    public abstract function getId();

    /**
     * @return DateTime | null
     */
    public function getUrlExpiresAt()
    {
        return $this->urlExpiresAt;
    }

    /**
     * @param DateTime $urlExpiresAt
     * @return $this
     */
    public function setUrlExpiresAt(?DateTime $urlExpiresAt): static
    {
        $this->urlExpiresAt = $urlExpiresAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(mixed $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return $this
     */
    public function setUrl(mixed $url): static
    {
        $this->url = $url;
        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     * @return $this
     */
    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }
}
