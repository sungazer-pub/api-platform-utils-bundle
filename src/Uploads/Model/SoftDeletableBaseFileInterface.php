<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model;


interface SoftDeletableBaseFileInterface
{
    public function isDeleted(): bool;
}