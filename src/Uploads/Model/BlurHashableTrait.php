<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model;


use Doctrine\ORM\Mapping as ORM;

trait BlurHashableTrait
{

    #[ORM\Column(type: 'text', nullable: true)]
    public string | null $blurHash;

    public function getBlurHash(): ?string {
        return $this->blurHash;
    }

    public function setBlurHash(?string $hash) {
        $this->blurHash = $hash;
        return $this;
    }
}
