<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model;


interface BlurHashableInterface
{
    public function getBlurHash(): ?string;

    public function setBlurHash(?string $hash): self;
}
