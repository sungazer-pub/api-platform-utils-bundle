<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Utils;


use kornrunner\Blurhash\Blurhash;

class BlurHashHelper
{
    public static function encode(string $filePath, array $opts = []): string
    {
        $opts   = array_merge([
            'componentsX'       => 4,
            'componentsY'       => 3,
            'maxImageDimension' => 64
        ],$opts);

        $img = new \Imagick($filePath);
        $img->scaleImage($opts['maxImageDimension'],$opts['maxImageDimension'],true);

        $width = $img->getImageWidth();
        $height = $img->getImageHeight();

        $pixels = [];
        for ($y = 0; $y < $height; ++$y) {
            $row = [];
            for ($x = 0; $x < $width; ++$x) {
                $px = $img->getImagePixelColor($x,$y);
                $c = $px->getColor();
                $row[] = [$c['r'],$c['g'],$c['b']];
            }
            $pixels[] = $row;
        }

        $blurhash     = Blurhash::encode($pixels, $opts['componentsX'],$opts['componentsY']);
        return $blurhash;

//        $size = self::getImageSizeKeepAspectRatio($filePath,$opts['maxImageDimension'],$opts['maxImageDimension']);
//        $contents = file_get_contents($filePath);
//        $image  = imagecreatefromstring($contents);
//        if($image === false){
//            return null;
//        }
//        $width  = imagesx($image);
//        $height = imagesy($image);
//        $thumb = imagecreatetruecolor($size['width'], $size['height']);
//        imagecopyresized($thumb, $image, 0, 0, 0, 0, $size['width'], $size['height'], $width, $height);
//        imagedestroy($image);
//
//        $width = $size['width'];
//        $height = $size['height'];
//
//        $pixels = [];
//        for ($y = 0; $y < $height; ++$y) {
//            $row = [];
//            for ($x = 0; $x < $width; ++$x) {
//                $index  = imagecolorat($thumb, $x, $y);
//                $colors = imagecolorsforindex($thumb, $index);
//
//                $row[] = [$colors['red'], $colors['green'], $colors['blue']];
//            }
//            $pixels[] = $row;
//        }
//        imagedestroy($thumb);
//
//        $blurhash     = Blurhash::encode($pixels, $opts['componentsX'],$opts['componentsY']);
//        return $blurhash;
    }

    // From https://gist.github.com/paulund/5244517
    private static function getImageSizeKeepAspectRatio( $imageUrl, $maxWidth, $maxHeight)
    {
        $imageDimensions = getimagesize($imageUrl);

        $imageWidth = $imageDimensions[0];
        $imageHeight = $imageDimensions[1];

        $imageSize['width'] = $imageWidth;
        $imageSize['height'] = $imageHeight;

        if($imageWidth > $maxWidth || $imageHeight > $maxHeight)
        {
            if ( $imageWidth > $imageHeight ) {
                $imageSize['height'] = floor(($imageHeight/$imageWidth)*$maxWidth);
                $imageSize['width']  = $maxWidth;
            } else {
                $imageSize['width']  = floor(($imageWidth/$imageHeight)*$maxHeight);
                $imageSize['height'] = $maxHeight;
            }
        }

        return $imageSize;
    }

}

