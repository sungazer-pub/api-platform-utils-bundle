<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Utils;


use Symfony\Component\HttpFoundation\Request;

class UploadHelper
{
    public const REQUEST_CONTEXT_KEY = '_sg_uploads_context';

    /**
     * @return array = [
     *     'files' => [
     *       'file' => 'UploadedFile',
     *       'otherkey' => 'UploadedFile'
     *     ]
     * ]
     */
    public static function getContextFromRequest(Request $request)
    {
        return $request->attributes->get(self::REQUEST_CONTEXT_KEY) ?? [];
    }

    /**
     * @param array $context = [
     *     'files' => [
     *       'file' => 'UploadedFile',
     *       'otherkey' => 'UploadedFile'
     *     ]
     * ]
     */
    public static function setContextFromRequest(Request $request, array $context): void
    {
        $request->attributes->set(self::REQUEST_CONTEXT_KEY, $context);
    }

}