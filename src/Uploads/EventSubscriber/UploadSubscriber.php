<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\EventSubscriber;


use ApiPlatform\Symfony\EventListener\EventPriorities;
use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use ReflectionClass;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Services\GenericClassMetadataExtractor;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Annotation\Uploadable;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\BaseFile;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Model\BlurHashableInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Utils\BlurHashHelper;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Uploads\Utils\UploadHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UploadSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface        $logger,
        private readonly FilesystemOperator     $storage,
        private readonly GenericClassMetadataExtractor $classMetadataExtractor,
    )
    {
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['preWrite', EventPriorities::PRE_WRITE]
            ]
        ];
    }

    public function preWrite(ViewEvent $event): void
    {
        /** @var BaseFile $data */
        $data = $event->getControllerResult();
        if (!($data instanceof BaseFile)) {
            return;
        }
        $request = $event->getRequest();
        if ($request->getMethod() === Request::METHOD_POST) {
            $context  = UploadHelper::getContextFromRequest($event->getRequest());
            $className = $data::class;

            /** @var Uploadable $annotation */
            $annotation = $this->classMetadataExtractor->extractOne($className, Uploadable::class);
            if(!$annotation){
                throw new \Exception("Missing Uploadable attribute on class $className");
            }

            /** @var UploadedFile $file */
            $file = $context['files']['file'];

            // Get path from root
            $filename = ($data->getId() ?? Uuid::uuid4()->toString()) . "." . $file->getClientOriginalExtension();
            $path     = joinPaths($annotation->prefix, $filename);

            if ($data instanceof BlurHashableInterface) {
                $start = microtime(true);
                $hash  = BlurHashHelper::encode($file->getRealPath());
                $data->setBlurHash($hash);
                $delta = microtime(true) - $start;
                $this->logger->debug("Computed blurhash in $delta seconds", ['file' => $data->getId()]);
            }

            // Perform upload
            $stream = fopen($file->getRealPath(), 'r+');
            $this->storage->writeStream($path, $stream);
            fclose($stream);

            $data->setPath($path);
            $data->setName($file->getClientOriginalName());
            $event->setControllerResult($data);
        }
    }
}

function joinPaths(...$args): array|string|null
{
    $paths = [];

    foreach ($args as $arg) {
        if ($arg !== '') {
            $paths[] = $arg;
        }
    }

    return preg_replace('#/+#', '/', join('/', $paths));
}
