<?php

namespace Sungazer\Bundle\ApiPlatformUtilsBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Purpose of this bundle is to gather common classes useful to work with api platform
 *
 * Author: Luca Nardelli <luca@sungazer.io>
 */

class SungazerApiPlatformUtilsBundle extends Bundle
{
    #[\Override]
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
    }
}

