<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 9.14
 */

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class SungazerApiPlatformUtilsExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     * @throws \Exception
     */
    #[\Override]
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();

        /** @var array $config = [
         *    "role_context" => true,
         *    "uploads" => [
         *      "enabled" => true,
         *      "uploader" => "flysystem service id"
         *    ]
         * ]
         */
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load("services.xml");

        if ($config['role_context']){
            $loader->load("role_context.xml");
        }
        if ($config['resource_dispatcher']['enabled']){
            $loader->load("api_resource_event_dispatcher.xml");
        }
        if ($config['uploads']['enabled']){
            $innerConfig = $config['uploads'];
            $container->setAlias('sungazer_api_platform_utils.flysystem_uploader',$innerConfig['uploader']);
            $loader->load("uploads.xml");
        }
        if ($config['documentation_patch']['enabled']){
            $loader->load("patched_documentation.xml");
            $innerConfig = $config['documentation_patch'];
            $routeNames = arrayToAssoc($innerConfig['route_names']);
            $container->setParameter('sungazer_api_platform_utils.documentation_patch.route_names', $routeNames);
        }
        if ($config['api_embedded']['enabled']){
            $loader->load("api_embedded.xml");
            $innerConfig = $config['api_embedded'];
            $container->setParameter('sungazer_api_platform_utils.embedded.embedded_directories', $innerConfig['directories']);
        }
        $loader->load("api_serialization_context.xml");
    }
}


/**
 * @return true[]
 */
function arrayToAssoc(array $values): array{
    $res = [];
    foreach($values as $value){
        $res[$value] = true;
    }
    return $res;
}
