<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 11.49
 */

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    #[\Override]
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_api_platform_utils');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode->children()
            ->scalarNode('role_context')
            ->defaultValue(true)->end();

        $rootNode->children()
            ->arrayNode('uploads')->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('enabled')->defaultValue(false)->end()
            ->scalarNode('uploader')->cannotBeEmpty()->end();

        $rootNode->children()
                 ->arrayNode('resource_dispatcher')->addDefaultsIfNotSet()
                 ->children()
                 ->scalarNode('enabled')->defaultValue(false)->end();

        $rootNode
            ->children()
                ->arrayNode('documentation_patch')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('enabled')->defaultValue(false)->end()
                    ->arrayNode('route_names')
                    ->scalarPrototype()->end()
                    ->defaultValue(['api_doc','api_jsonld_context'])
                    ->end()
                ->end()
            ->end();

        $rootNode
            ->children()
            ->arrayNode('api_embedded')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('enabled')->defaultValue(true)->end()
            ->arrayNode('directories')
            ->scalarPrototype()->end()
            ->defaultValue(['%kernel.project_dir%/src'])
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
