<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\PatchedDocumentation;


use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class DocumentationPropertyMetadataFactory implements PropertyMetadataFactoryInterface
{
  /**
   * @param mixed[] $allowedRouteNames
   */
  public function __construct(
    private readonly PropertyMetadataFactoryInterface $decorated,
    private readonly RequestStack                     $requestStack,
    private                                           $allowedRouteNames,
  )
  {
  }

  #[\Override]
  public function create(string $resourceClass, string $property, array $options = []): ApiProperty
  {
    $obj = $this->decorated->create($resourceClass, $property, $options);

    // Only patch the metadata for documentation routes
    $request = $this->requestStack->getMainRequest();
    if (!$request) {
      return $obj;
    }
    $route = $request->attributes->get('_route', null);
    if (array_key_exists($route, $this->allowedRouteNames)) {
      $obj = $obj
        ->withWritable(true)
        ->withReadable(true);
    }
    return $obj;
  }
}
