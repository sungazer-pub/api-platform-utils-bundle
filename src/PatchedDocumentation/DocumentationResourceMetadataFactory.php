<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\PatchedDocumentation;


use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\Metadata\Resource\ResourceMetadataCollection;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;

class DocumentationResourceMetadataFactory implements ResourceMetadataCollectionFactoryInterface
{
  /**
   * @param mixed[] $allowedRouteNames
   */
  public function __construct(
    private readonly ResourceMetadataCollectionFactoryInterface $decorated,
    private readonly ClassMetadataFactoryInterface              $classMetadataFactory,
    private readonly RequestStack                               $requestStack,
    private                                                     $allowedRouteNames,)
  {
  }

  #[\Override]
  public function create(string $resourceClass): ResourceMetadataCollection
  {
    $resourceMetadataCollection = $this->decorated->create($resourceClass);
    /** @var ApiResource $obj */
    $obj = &$resourceMetadataCollection[0];

    // Only patch the metadata for documentation routes
    $request = $this->requestStack->getMainRequest();
    if (!$request) {
      return $resourceMetadataCollection;
    }
    $route = $request->attributes->get('_route', null);
    if (array_key_exists($route, $this->allowedRouteNames)) {
      $groups = [];
      foreach ($this->classMetadataFactory->getMetadataFor($resourceClass)->getAttributesMetadata() as $attributeMetadata) {
        foreach ($attributeMetadata->getGroups() as $group) {
          $groups[$group] = true;
        }
      }
      $groups                                    = array_keys($groups);

      $normalizationContext = $obj->getNormalizationContext() ?? [];
      $denormalizationContext = $obj->getDenormalizationContext() ?? [];

      $normalizationContext['groups'] = $groups;
      $denormalizationContext['groups'] = $groups;

      $obj = $obj->withDenormalizationContext($denormalizationContext)
                 ->withNormalizationContext($normalizationContext);
      $resourceMetadataCollection[0] = $obj;
    }
    return $resourceMetadataCollection;
  }
}
