<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\EventSubscriber;


use ApiPlatform\Symfony\EventListener\EventPriorities;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;

use ApiPlatform\Symfony\EventListener\SerializeListener;
use ApiPlatform\Symfony\EventListener\ValidateListener;
use ApiPlatform\Util\OperationRequestInitiatorTrait;
use ApiPlatform\Util\RequestAttributesExtractor;
use Psr\Log\LoggerInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event\ApiResourcePostValidateEvent;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event\ApiResourcePostWriteEvent;
use Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event\ApiResourcePreWriteEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ApiResourceDispatcherEventSubscriber implements EventSubscriberInterface
{
    use OperationRequestInitiatorTrait;

    public function __construct(
        private LoggerInterface                  $logger,
        private EventDispatcherInterface         $eventDispatcher,
        ResourceMetadataCollectionFactoryInterface $resourceMetadataCollectionFactory,
    )
    {
        $this->resourceMetadataCollectionFactory = $resourceMetadataCollectionFactory;
    }

    /**
     * @inheritDoc
     */
    #[\Override]
    public static function getSubscribedEvents(): array
    {
        $events = [
            KernelEvents::VIEW => [
                ['postValidate', EventPriorities::POST_VALIDATE],
                ['preWrite', EventPriorities::PRE_WRITE],
                ['postWrite', EventPriorities::POST_WRITE],
            ]
        ];
        return $events;
    }

    public function postValidate(ViewEvent $event): void
    {
        if (!$this->validateListenerExecutes($event)) {
            return;
        }

        $class = $this->getClassName($event->getControllerResult());
        /** @var ApiResourcePostValidateEvent $res */
        $res = $this->eventDispatcher->dispatch(
            new ApiResourcePostValidateEvent($event->getControllerResult(), $event),
            ApiResourcePostValidateEvent::getEventForClass($class)
        );
        $event->setControllerResult($res->getEntity());
    }

    private function validateListenerExecutes(ViewEvent $event): bool
    {
        $controllerResult = $event->getControllerResult();
        $request          = $event->getRequest();
        $operation        = $this->initializeOperation($request);

        if (
            $controllerResult instanceof Response
            || $request->isMethodSafe()
            || $request->isMethod('DELETE')
            || !($attributes = RequestAttributesExtractor::extractAttributes($request))
        ) {
            return false;
        }

        if ($this->resourceMetadataCollectionFactory instanceof ResourceMetadataCollectionFactoryInterface &&
            (!$operation || !($operation->canValidate() ?? true))
        ) {
            return false;
        }

        // TODO: 3.0 remove condition
        if (
            $this->resourceMetadataCollectionFactory instanceof ResourceMetadataFactoryInterface && (
                !$attributes['receive']
                || $this->isOperationAttributeDisabled(
                    $attributes,
                    ValidateListener::OPERATION_ATTRIBUTE_KEY
                )
            )
        ) {
            return false;
        }
        return true;
    }

    private function isOperationAttributeDisabled(array  $attributes,
                                                  string $attribute,
                                                  bool   $default = false,
                                                  bool   $resourceFallback = true): bool
    {
        if (null === $this->resourceMetadataCollectionFactory) {
            return $default;
        }

        $resourceMetadata = $this->resourceMetadataCollectionFactory->create($attributes['resource_class']);

        return !((bool)$resourceMetadata->getOperationAttribute($attributes, $attribute, !$default, $resourceFallback));
    }

    private function getClassName($entity): array|string
    {
        $class = $entity::class;
        // Sanitize for doctrine proxies
        $class = str_replace(['Proxies\\__CG__\\'], '', (string) $class);
        return $class;
    }

    public function preWrite(ViewEvent $event): void
    {
        if (!$this->writeListenerExecutes($event)) {
            return;
        }

        $class = $this->getClassName($event->getControllerResult());
        /** @var ApiResourcePreWriteEvent $res */
        $res = $this->eventDispatcher->dispatch(
            new ApiResourcePreWriteEvent($event->getControllerResult(), $event),
            ApiResourcePreWriteEvent::getEventForClass($class)
        );
        $event->setControllerResult($res->getEntity());
    }

    // Adapted from vendor/api-platform/core/src/Symfony/EventListener/WriteListener.php
    private function writeListenerExecutes(ViewEvent $event): bool
    {
        $controllerResult = $event->getControllerResult();
        $request          = $event->getRequest();
        $operation        = $this->initializeOperation($request);

        if (
            $controllerResult instanceof Response
            || $request->isMethodSafe()
            || !($attributes = RequestAttributesExtractor::extractAttributes($request))
        ) {
            return false;
        }

        if (!$operation
            || ($operation->getExtraProperties()['is_legacy_resource_metadata'] ?? false)
            || !($operation->canWrite() ?? true) || !$attributes['persist']) {
            return false;
        }

        if (!$operation->getProcessor()) {
            return false;
        }
        return true;
    }

    public function postWrite(ViewEvent $event): void
    {
        if (!$this->writeListenerExecutes($event)) {
            return;
        }

        $result = $event->getControllerResult();
        if (!$result) {
            return;
        }
        $class = $this->getClassName($result);
        /** @var ApiResourcePostWriteEvent $res */
        $res = $this->eventDispatcher->dispatch(
            new ApiResourcePostWriteEvent($event->getControllerResult(), $event),
            ApiResourcePostWriteEvent::getEventForClass($class)
        );
        $event->setControllerResult($res->getEntity());
    }

    // Adapted from vendor/api-platform/core/src/Symfony/EventListener/SerializeListener.php
    private function serializeListenerExecutes(ViewEvent $event): bool
    {
        $controllerResult = $event->getControllerResult();
        $request          = $event->getRequest();
        $operation        = $this->initializeOperation($request);

        if ($controllerResult instanceof Response) {
            return false;
        }

        $attributes = RequestAttributesExtractor::extractAttributes($request);

        // TODO: 3.0 adapt condition (remove legacy part)
        if (
            !($attributes['respond'] ?? $request->attributes->getBoolean('_api_respond', false))
            || (
                (!$this->resourceMetadataCollectionFactory || $this->resourceMetadataCollectionFactory instanceof ResourceMetadataFactoryInterface)
                && $attributes
                && $this->isOperationAttributeDisabled(
                    $attributes,
                    SerializeListener::OPERATION_ATTRIBUTE_KEY
                )
            )
        ) {
            return false;
        }

        if ($this->resourceMetadataCollectionFactory instanceof ResourceMetadataCollectionFactoryInterface &&
            ($operation && !($operation->canSerialize() ?? true))
        ) {
            return false;
        }

        return true;
    }

}
