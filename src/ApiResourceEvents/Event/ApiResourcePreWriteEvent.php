<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event;


use Symfony\Component\HttpKernel\Event\ViewEvent;

class ApiResourcePreWriteEvent extends ApiResourceViewEvent
{

    public function __construct($entity, ViewEvent $event)
    {
        parent::__construct($entity,$event);
    }

    #[\Override]
    public static function getBaseEventName(): string
    {
        return 'preWrite';
    }
}