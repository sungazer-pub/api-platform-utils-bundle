<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event;


use Symfony\Component\HttpKernel\Event\ViewEvent;

/**
 *
 */
abstract class ApiResourceViewEvent
{
    public function __construct(protected $entity, protected \Symfony\Component\HttpKernel\Event\ViewEvent $event)
    {
    }

    public static function getEventForClass(string $class): string
    {
        return sprintf("%s.%s", $class, static::getBaseEventName());
    }

    public abstract static function getBaseEventName(): string;

    public function getEvent(): ViewEvent
    {
        return $this->event;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function setEntity(mixed $entity): void
    {
        $this->entity = $entity;
    }

}