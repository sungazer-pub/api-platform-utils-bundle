<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\ApiResourceEvents\Event;


use Symfony\Component\HttpKernel\Event\ViewEvent;

class ApiResourcePostValidateEvent extends ApiResourceViewEvent
{

    public function __construct($entity, ViewEvent $event)
    {
        parent::__construct($entity,$event);
    }

    #[\Override]
    public static function getBaseEventName(): string
    {
        return 'postValidate';
    }
}