<?php

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\CustomFilter;

use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CustomFilter
 * @package App\Filter
 * This filter is only applied to classes as a whole
 */
final class CustomFilter extends AbstractFilter
{

    /**
     * Gets the description of this filter for the given resource.
     *
     * Returns an array with the filter parameter names as keys and array with the following data as values:
     *   - property: the property where the filter is applied
     *   - type: the type of the filter
     *   - required: if this filter is required
     *   - strategy: the used strategy
     *   - is_collection (optional): is this filter is collection
     *   - swagger (optional): additional parameters for the path operation,
     *     e.g. 'swagger' => [
     *       'description' => 'My Description',
     *       'name' => 'My Name',
     *       'type' => 'integer',
     *     ]
     *   - openapi (optional): additional parameters for the path operation in the version 3 spec,
     *     e.g. 'openapi' => [
     *       'description' => 'My Description',
     *       'name' => 'My Name',
     *       'schema' => [
     *          'type' => 'integer',
     *       ]
     *     ]
     * The description can contain additional data specific to a filter.
     *
     * @see \ApiPlatform\Core\Swagger\Serializer\DocumentationNormalizer::getFiltersParameters
     */
    #[\Override]
    public function getDescription(string $resourceClass): array
    {
        $description = [
            "q" => [
                'property' => null,
                'type'     => 'string',
                'required' => false,
            ]
        ];
        return $description;
    }

    /**
     * Passes a property through the filter.
     * @param string $property
     * @param $value
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param string|null $operationName
     */
    #[\Override]
    protected function filterProperty(
      string $property,
      $value,
      QueryBuilder $queryBuilder,
      QueryNameGeneratorInterface $queryNameGenerator,
      string $resourceClass,
      ?Operation $operation = null,
      array $context = []
    ): void
    {
        if($property !== 'q'){
            return;
        }
        $filterData = json_decode((string) $value, true, 512, JSON_THROW_ON_ERROR);

        if(is_array($filterData)) {
            foreach ($filterData as $filter) {
                $queryBuilder->andWhere($this->parseFilter($filter, $queryBuilder, $queryNameGenerator, $resourceClass));
            }
        } else {
            $queryBuilder->andWhere($this->parseFilter($filterData, $queryBuilder, $queryNameGenerator, $resourceClass));
        }
//        $this->logger->info(sprintf("CustomFilter DQL: %s", $queryBuilder->getDQL()));
    }

    private function parseFilter(array $filter, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): string
    {
//        $this->logger->info("Parsing operation " . json_encode($filter));
        $op  = $filter['op'];
        $val = $filter['val'] ?? null;

        // Handle OR case
        if ($op === 'OR') {
            $parts       = [];
            $queryString = "";
            foreach ($val as $subFilter) {
                $parts[] = $this->parseFilter($subFilter, $queryBuilder, $queryNameGenerator, $resourceClass);
            }
            $queryString .= " (" . join(" OR ", $parts) . ")";
            return $queryString;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $prop      = $filter['prop'];

        if ($this->isPropertyNested($prop, $resourceClass)) {
            [$alias, $field, $associations] = $this->addJoinsForNestedProperty($prop, $rootAlias, $queryBuilder, $queryNameGenerator, $resourceClass,Join::LEFT_JOIN);
        } else {
            $alias        = $rootAlias;
            $field        = $prop;
            $associations = [];
        }
        $metadata = $this->getNestedMetadata($resourceClass, $associations);

        $queryString = null;
        switch ($op) {
            case 'NOT_EQUAL':
            case 'EQUAL':
                $dqlOp = $op === 'EQUAL'? '=' : '!=';
                $param       = $queryNameGenerator->generateParameterName($prop);
                $queryString = " $alias.$field $dqlOp :$param";
                $queryBuilder->setParameter(":$param", $val);
                break;
            case 'NOT_IN':
            case 'IN':
                $dqlOp = $op === 'IN'? 'IN' : 'NOT IN';
                $param       = $queryNameGenerator->generateParameterName($prop);
                $queryString = " $alias.$field $dqlOp (:$param)";
                $queryBuilder->setParameter(":$param", $val);
                break;
            case 'ILIKE':
                $param       = $queryNameGenerator->generateParameterName($prop);
                $queryString = " LOWER($alias.$field) LIKE LOWER(:$param)";
                $queryBuilder->setParameter(":$param", "%$val%");
                break;
            case 'LIKE':
                $param       = $queryNameGenerator->generateParameterName($prop);
                $queryString = " $alias.$field LIKE :$param";
                $queryBuilder->setParameter(":$param", "%$val%");
                break;
            case 'EXISTS':
                if ($metadata->hasAssociation($field)) {
                    if ($metadata->isCollectionValuedAssociation($field)) {
                        $queryString = sprintf('%s.%s %s EMPTY', $alias, $field, $val ? 'IS NOT' : 'IS');
                        break;
                    }
                    if ($metadata->isAssociationInverseSide($field)) {
                        $alias = QueryBuilderHelper::addJoinOnce($queryBuilder, $queryNameGenerator, $alias, $field, Join::LEFT_JOIN);

                        $queryString = sprintf('%s %s NULL', $alias, $val ? 'IS NOT' : 'IS');

                        break;
                    }
                    $queryString = sprintf('%s.%s %s NULL', $alias, $field, $val ? 'IS NOT' : 'IS');
                    break;
                }

                if ($metadata->hasField($field)) {
                    $queryString = sprintf('%s.%s %s NULL', $alias, $field, $val ? 'IS NOT' : 'IS');
                }
                break;

        }
        return $queryString;
    }
}
