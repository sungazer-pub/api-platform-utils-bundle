<?php

namespace Sungazer\Bundle\ApiPlatformUtilsBundle\Services;

use Doctrine\Common\Annotations\Reader;
use Psr\Cache\CacheItemPoolInterface;
use ReflectionClass;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Cache\CachedTrait;

/**
 * Generic metadata extractor to extract annotations/attributes from class
 */
class GenericClassMetadataExtractor
{
    use CachedTrait;

    public function __construct(
        private readonly ?Reader                 $reader,
        CacheItemPoolInterface $cacheItemPool,
    )
    {
        $this->cacheItemPool = $cacheItemPool;
    }

    public function extract(string $className, string $annotationClass): array
    {
        return $this->getCached(
            sprintf("%s.%s", md5($className), md5($annotationClass)),
            function () use ($className, $annotationClass): array {
                $refClass = new ReflectionClass($className);
                // Try reading attribute first, then annotation
                $attributes = $refClass->getAttributes($annotationClass);
                if (count($attributes) > 0) {
                    return array_map(fn(\ReflectionAttribute $attr): object => $attr->newInstance(), $attributes);
                }
                if($this->reader) {
                  $annotations = $this->reader->getClassAnnotations($refClass);
                  if (!$annotations) {
                    return [];
                  }
                  // Filter annotations that are not ours
                  $annotations = array_filter($annotations, fn($ann): bool => $ann::class === $annotationClass);
                  return $annotations;
                }
                return [];
            }
        );
    }

    public function extractOne(string $className, string $annotationClass): mixed {
        $annotations = $this->extract($className,$annotationClass);
        return $annotations[0] ?? null;
    }
}
