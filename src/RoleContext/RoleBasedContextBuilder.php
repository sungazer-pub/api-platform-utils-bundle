<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\RoleContext;

use ApiPlatform\Serializer\SerializerContextBuilderInterface;
use Sungazer\Bundle\ApiPlatformUtilsBundle\RoleContext\Annotation\RoleContext;
use Sungazer\Bundle\ApiPlatformUtilsBundle\Services\GenericClassMetadataExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final readonly class RoleBasedContextBuilder implements SerializerContextBuilderInterface
{
    public function __construct(
        private SerializerContextBuilderInterface $decorated,
        private AuthorizationCheckerInterface     $authorizationChecker,
        private GenericClassMetadataExtractor     $extractor,
        private TokenStorageInterface             $tokenStorageInterface,
    )
    {
    }

    #[\Override]
    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context       = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        // Don't do anything if user is not logged in
        if (null !== $this->tokenStorageInterface->getToken()) {
            $metadataArray = $this->extractor->extract($resourceClass, RoleContext::class);
            if ($metadataArray) {
                foreach ($metadataArray as $metadata) {
//                error_log("RoleContext: testing for role $metadata->role");
                    if ($this->authorizationChecker->isGranted($metadata->role)) {
                        if ($normalization && array_key_exists('groups', $metadata->normalizationContext)) {
//                        error_log("Applying normalization groups " . join(",", $metadata->normalizationContext['groups']) . " to resource $resourceClass");
                            $context['groups'] = array_merge(
                                $context['groups'],
                                $metadata->normalizationContext['groups']
                            );
                        } else if (!$normalization && array_key_exists('groups', $metadata->denormalizationContext)) {
//                        error_log("Applying denormalization groups " . join(",", $metadata->denormalizationContext['groups']) . " to resource $resourceClass");
                            $context['groups'] = array_merge(
                                $context['groups'],
                                $metadata->denormalizationContext['groups']
                            );
                        }
                    }
                }
            }
        }

        return $context;
    }
}
