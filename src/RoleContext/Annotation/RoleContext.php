<?php


namespace Sungazer\Bundle\ApiPlatformUtilsBundle\RoleContext\Annotation;


use Attribute;
use Doctrine\Common\Annotations\Annotation\NamedArgumentConstructor;
use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\ORM\Mapping\MappingAttribute;

/**
 * @Annotation
 * @Target("CLASS")
 * @NamedArgumentConstructor()
 */
#[Attribute(Attribute::TARGET_CLASS)]
class RoleContext implements MappingAttribute
{
    public function __construct(
        public string $role,
        public array  $normalizationContext,
        public array  $denormalizationContext,
    )
    {
    }
}
